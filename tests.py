import unittest
from main import app





class MainTest(unittest.TestCase):

    def test_index(self):
        tester = app.test_client(self)
        res = tester.get("/")
        statuscode = res.status_code
        self.assertEqual(statuscode, 200)





if __name__ == "__main__":
    unittest.main()
